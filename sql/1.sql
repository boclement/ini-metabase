SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de données :  `meta_initiative`
--

-- --------------------------------------------------------

--
-- Structure de la table `crmentity`
--

CREATE TABLE `crmentity` (
  `instanceid` int(11) NOT NULL,
  `module` varchar(30) NOT NULL,
  `week` date NOT NULL,
  `nb_create` int(11) NOT NULL DEFAULT '0',
  `nb_modify` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `instances`
--

CREATE TABLE `instances` (
  `instanceid` int(10) NOT NULL,
  `instance` varchar(50) NOT NULL,
  `prefix` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `login`
--

CREATE TABLE `login` (
  `instanceid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `week` date NOT NULL,
  `nb_connections` int(5) NOT NULL DEFAULT '0',
  `user_name` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `instanceid` int(10) NOT NULL,
  `week` date NOT NULL,
  `nb_users_dans_panel` int(5) NOT NULL,
  `nb_users_actifs` int(5) NOT NULL,
  `nb_users_inactifs` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Index pour la table `crmentity`
--
ALTER TABLE `crmentity`
  ADD UNIQUE KEY `UNIQUE` (`instanceid`,`module`,`week`);

--
-- Index pour la table `instances`
--
ALTER TABLE `instances`
  ADD PRIMARY KEY (`instanceid`);

--
-- Index pour la table `login`
--
ALTER TABLE `login`
  ADD UNIQUE KEY `UNIQUE` (`instanceid`,`userid`,`week`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `UNIQUE` (`instanceid`,`week`);

COMMIT;
