#!/usr/bin/env php
<?php

require('scripts/Main.php');
require('scripts/Database.php');
require('scripts/Consolidate.php');
require('scripts/ConnectionStats.php');

global $argv;

$mode = 'LAST_WEEK';

for ($i = 1; $i < count($argv); $i++) {
    switch ($argv[$i]) {
        case '-history':
            $mode = 'HISTORY';
            break;
        default :
            break;
    }
}

$Main = new Main();
$Main->getInstances();
$Main->processMetaData($mode); // with parameter "HISTORY" to build a 2 year old history