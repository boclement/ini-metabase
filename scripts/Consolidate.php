<?php
/**
 * Created by PhpStorm.
 * User: boris
 * Date: 03/07/19
 * Time: 16:17
 */

class Consolidate {

    private $altDbConnexion = '';
    private $dbName;
    private $idInstance;
    private $nbUsers;
    private $mode = 'LAST_WEEK'; // OR 'FIRST_WEEK_OR' first week of year

    private $weekHistory;

    public function __construct(array $config)
    {
        $this->dbName = $config['db_name'];
        $this->idInstance = $config['id'];
        $this->nbUsers = $config['nb_users'];
        $this->connect();
    }

    private function connect() {

        $dbconfig = require('config/database.php');
        $dsn = 'mysql:dbname='.$this->dbName.';host='.$dbconfig['host'];
        try {
            $this->altDbConnexion = new PDO($dsn, $dbconfig['username'], $dbconfig['password']);
        } catch (PDOException $e) {
            echo 'Connexion échouée : ' . $e->getMessage();
        }
    }


    public function process($mode) {
        $this->mode = $mode;
        if ($this->mode == 'LAST_WEEK') {
            $this->manageEntity();
            $this->manageLoginHistory();
            $this->manageActiveUsers();
        } else {
            for ($i = 0; $i < 100; $i++) {
                $this->weekHistory = $i;
                $this->manageEntity();
                $this->manageLoginHistory();
            }
            $this->manageActiveUsers();
        }
    }


    public function getPreviousWeeklyData()
    {
        if ($this->mode == 'LAST_WEEK') {
            return [
                'date_start' => date('Y-m-d', strtotime("last Monday - 1 week ")),
                'date_end' => date('Y-m-d', strtotime("last Monday "))
            ];

        } elseif ($this->mode == 'HISTORY') {
            return [
                'date_start' => date('Y-m-d', strtotime("last Monday - ".($this->weekHistory+1)." weeks")),
                'date_end' => date('Y-m-d', strtotime("last Monday- ".$this->weekHistory." weeks"))
            ];
        }
    }


    protected function manageEntity () {
        $replaceQuery = 'REPLACE INTO crmentity (instanceid, module, `week`, nb_create, nb_modify) VALUES  ';

        $dates = $this->getPreviousWeeklyData();
        $datas = [];
        // manage create
        $query = "SELECT COUNT(*) AS nb_item, setype AS module FROM `vtiger_crmentity` 
          WHERE deleted = 0 AND createdtime BETWEEN '".$dates['date_start']."' AND '".$dates['date_end']."' GROUP BY setype";

        foreach ($this->altDbConnexion->query($query) as $row) {
            $datas[$row['module']] = [$this->idInstance, "'".$row['module']."'", "'".$dates['date_start']."'", $row['nb_item'], 0];
        }

        // manage modify
        $query = "SELECT COUNT(*) AS nb_item, setype AS module FROM `vtiger_crmentity` 
          WHERE deleted = 0 AND modifiedtime BETWEEN '".$dates['date_start']."' AND '".$dates['date_end']."' GROUP BY setype";

        foreach ($this->altDbConnexion->query($query) as $row) {
            if (isset($datas[$row['module']])) {
                $datas[$row['module']][4] = $row['nb_item'];
            } else {
                $datas[$row['module']] = [$this->idInstance, "'" . $row['module'] . "'", "'" . $dates['date_start'] . "'", 0, $row['nb_item']];
            }
        }
        foreach ($datas as $data) {
            Database::getConnexion()->query($replaceQuery . '(' . implode(",", $data) . ')');
        }
    }


    protected function manageEntitybyUser () {
        $query = "SELECT COUNT(*) AS nb_item, setype AS module, smownerid FROM `vtiger_crmentity` WHERE deleted = 0 AND createdtime > 0 GROUP BY setype, smownerid";
    }


    protected function manageLoginHistory() {
        $insertQuery = 'REPLACE INTO login (instanceid, userid, `week`, nb_connections, user_name) VALUES  ';

        $dates = $this->getPreviousWeeklyData();
        $datas = [];

        $query = "SELECT COUNT(*) AS nb_connections, vtiger_loginhistory.user_name,id FROM `vtiger_loginhistory` 
          JOIN vtiger_users ON vtiger_users.user_name = vtiger_loginhistory.user_name 
          WHERE login_time BETWEEN '".$dates['date_start']."' AND '".$dates['date_end']."' 
          GROUP BY vtiger_loginhistory.user_name,vtiger_users.id ";

        foreach ($this->altDbConnexion->query($query) as $row) {
            $datas[$row['id']] = [$this->idInstance, "'".$row['id']."'", "'".$dates['date_start']."'", $row['nb_connections'], "'".substr($row['user_name'],0,2)."'"];
        }
        foreach ($datas as $data) {
            Database::getConnexion()->query($insertQuery . '(' . implode(",", $data) . ')');
        }
    }


    protected function manageActiveUsers() {
        $insertQuery = 'REPLACE INTO `users` (`instanceid`, `nb_users_dans_panel`, `nb_users_actifs`, `nb_users_inactifs`, `week`) VALUES ';
        $dates = $this->getPreviousWeeklyData();

        $query = 'SELECT count(*) AS nb_active_users FROM `vtiger_users` WHERE deleted = 0 AND status = \'Active\'';
        foreach ($this->altDbConnexion->query($query) as $row) {
            $active = $row['nb_active_users'];
        }
        $query = 'SELECT count(*) AS nb_inactive_users FROM `vtiger_users` WHERE status = \'Inactive\'';
        foreach ($this->altDbConnexion->query($query) as $row) {
            $inactive = $row['nb_inactive_users'];
        }
        $data= [$this->idInstance, $this->nbUsers, $active, $inactive, "'".$dates['date_start']."'"];
        Database::getConnexion()->query($insertQuery . '(' . implode(",", $data) . ')');
    }


}