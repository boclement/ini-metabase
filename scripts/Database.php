<?php

class Database {

    static $dbConnexion;

    /**
     * @return PDO
     */
    static function getConnexion() {
        if (!isset(Database::$dbConnexion)) {
            $dbconfig = require('config/database.php');
            try {
                Database::$dbConnexion = new PDO($dbconfig['dsn'], $dbconfig['username'], $dbconfig['password']);
            } catch (PDOException $e) {
                echo 'Connexion échouée : ' . $e->getMessage();
            }
        }
        return Database::$dbConnexion;
    }

    public function query($query){
        return (Database::$dbConnexion)->query($query);
    }
}