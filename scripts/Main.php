<?php
/**
 * Created by PhpStorm.
 * User: boris
 * Date: 03/07/19
 * Time: 16:16
 */

class Main {

    private $instances ;
    private $dbConnexion ; /** var PDO $dbConnexion  */

    public function __construct()
    {
        $this->getConnexion();
    }

    private function getConnexion() {
        if (!isset(Database::$dbConnexion)) {
            $dbconfig = require('config/database-imanagerapi.php');
            try {
                $this->dbConnexion = new PDO($dbconfig['dsn'], $dbconfig['username'], $dbconfig['password']);
            } catch (PDOException $e) {
                echo 'Connexion échouée : ' . $e->getMessage();
            }
        }
        return Database::$dbConnexion;
    }

    public function getInstances() {

        $query = 'SELECT instanceid, prefix_login, entreprise, db_name, nb_users, accountid_crm  FROM instances WHERE active = 1';
        foreach  ($this->dbConnexion->query($query) as $row) {
            $this->instances[$row['instanceid']] = [
                'id' => $row['instanceid'],
                'db_name' => $row['db_name'],
                'nb_users' => $row['nb_users'],
                'accountid_crm' => $row['accountid_crm']
            ];
            $this->setInstance($row);
        }
    }

    private function setInstance($row) {
        $query = "REPLACE INTO instances (instanceid, instance, prefix, accountid_crm) 
        VALUES (".$row['instanceid'].",'".$row['entreprise']."','".$row['prefix_login']."' ,'".$row['accountid_crm']."') ";
        Database::getConnexion()->query($query);
    }

    public function processMetaData($mode = 'LAST_WEEK') {
        foreach($this->instances as $instance) {
            $consolidate = new Consolidate($instance);

            $consolidate->process($mode);

            $stats = new ConnectionStats($instance);
            $stats->buildData();
            $stats->compareData();
            $stats->storeData();

        }
    }
}