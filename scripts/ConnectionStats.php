<?php

class ConnectionStats {

    protected  $instanceid;
    protected  $accountid;

    protected $querylists = [];

    public $updatefields = [
        'cf_1316' => '', // nb connexion semaine S
        'cf_1317' => '', // nb user connecté semaine S
        'cf_1318' => '', // Nb users actifs
        'cf_1319' => '', // Nb users inactifs
        'cf_1320' => '', // nb connexion semaine S - 1
        'cf_1321' => '', // nb connexion semaine S - 4
        'cf_1322' => '', // nb user connecté semaine S - 1
        'cf_1323' => '', // nb user connecté semaine S - 4
    ];

    /**
     * @return mixed
     */
    public function __construct($instance)
    {
        $this->instanceid = $instance['id'];
        $this->accountid = $instance['accountid_crm'];
        $this->setQueries();
    }

    public function buildData() {
        $previousWeek = $this->getPreviousWeeklyData();
        foreach ($this->querylists as $kpi => $sql) {

            $query = Database::getConnexion()->prepare($sql);
            $query->execute([$this->instanceid, $previousWeek['date_start']]);
            $result = $query->fetchAll();

            $this->updatefields[$kpi] = isset($result[0][0]) ? $result[0][0] : 0;
        }
    }

    public function compareData() {

        // compare with previous week
        $previousWeek = $this->getLastPreviousWeeklyData();

        // nb of connection
        $query = Database::getConnexion()->prepare($this->querylists['cf_1316']);
        $query->execute([$this->instanceid, $previousWeek['date_start']]);
        $result = $query->fetchAll();
        $temp_val = (int)isset($result[0][0]) ? $result[0][0] : 0;
        $this->updatefields['cf_1320'] = $temp_val > 0 ? (($this->updatefields['cf_1316'] - $temp_val) / $temp_val) * 100 : '';

        // nb of distinct connection by user
        $query = Database::getConnexion()->prepare($this->querylists['cf_1317']);
        $query->execute([$this->instanceid, $previousWeek['date_start']]);
        $result = $query->fetchAll();
        $temp_val = (int)isset($result[0][0]) ? $result[0][0] : 0;
        $this->updatefields['cf_1322'] = $temp_val > 0 ? (($this->updatefields['cf_1317'] - $temp_val) / $temp_val) * 100 : '';

        // compare with week S -4
        $previousMonthlyWeek = $this->getPreviousMonthWeeklyData();

        // nb of connection
        $query = Database::getConnexion()->prepare($this->querylists['cf_1316']);
        $query->execute([$this->instanceid, $previousMonthlyWeek['date_start']]);
        $result = $query->fetchAll();
        $temp_val = (int)isset($result[0][0]) ? $result[0][0] : 0;
        $this->updatefields['cf_1321'] = $temp_val > 0 ? (($this->updatefields['cf_1316'] - $temp_val) / $temp_val) * 100 : '';

        // nb of distinct connection by user
        $query = Database::getConnexion()->prepare($this->querylists['cf_1317']);
        $query->execute([$this->instanceid, $previousMonthlyWeek['date_start']]);
        $result = $query->fetchAll();
        $temp_val = (int)isset($result[0][0]) ? $result[0][0] : 0;
        $this->updatefields['cf_1323'] = $temp_val > 0 ? (($this->updatefields['cf_1317'] - $temp_val) / $temp_val) * 100 : '';

    }

    public function storeData() {

        $sqlupdate = 'UPDATE vtiger_accountscf SET ';

        foreach ($this->updatefields as $kpi => $value) {
            $sqlupdate .= $kpi." = '".$value."' ,";
        }
        $sqlupdate = trim($sqlupdate,',');
        $sqlupdate .= ' WHERE accountid = '.$this->accountid.';<br>';
        echo $sqlupdate;
    }

    public function setQueries()
    {

        $this->querylists = [
            'cf_1316' => 'SELECT SUM(nb_connections) AS nb_connections FROM `login` WHERE instanceid = ? AND week =  ?  ',
            'cf_1317' => 'SELECT COUNT(DISTINCT(user_name)) AS nb_distinct_connections FROM `login` WHERE  instanceid = ? AND week =  ?  ',
            'cf_1318' => 'SELECT nb_users_actifs AS nb_active_users FROM `users` WHERE  instanceid = ? AND week =  ?   ',
            'cf_1319' => 'SELECT nb_users_inactifs AS nb_inactive_users FROM `users` WHERE  instanceid = ? AND week =  ?  ',
        ];
        // 'nb_connections_by_user' => 'SELECT user_name, COUNT(*) AS nb_connections_by_user FROM `vtiger_loginhistory` WHERE login_time BETWEEN ? AND ? GROUP BY user_name',
    }

    public function getPreviousWeeklyData() {
        return [
            'date_start' => date('Y-m-d', strtotime("last Monday - 1 week")),
            'date_end' => date('Y-m-d', strtotime("last Monday"))
        ];
    }

    public function getLastPreviousWeeklyData() {
        return [
            'date_start' => date('Y-m-d', strtotime("last Monday - 2 weeks")),
            'date_end' => date('Y-m-d', strtotime("last Monday - 1 week"))
        ];
    }

    public function getPreviousMonthWeeklyData() {
        return [
            'date_start' => date('Y-m-d', strtotime("last Monday - 5 weeks")),
            'date_end' => date('Y-m-d', strtotime("last Monday - 4 weeks"))
        ];
    }
}
